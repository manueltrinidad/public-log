# Python TCP Server Readme

## Intro
- It's a basic TCP server that will take requests on localhost:12345
- It logs basic actions using PowerShell's Moduse POSH.

## Instructions
- **Install POSH by running lab3-itcollege-vm-mgmt.ps1 at least once.**
- Start the TCP server by running _log-lab3.py_ using Python3.

## Functionality
- Once the python file has been executed, it will create a TCP server.
- The following actions will be logged:
	- Starting the server
	- Closing the server with a SIGINT
	- Getting a request to the server
- Once any of these actions is taken, the parameters will be parsed into a **Send-SyslogMessage** command.
- The python file will use POSH, which should be installed in the computer, to parse the logging.
- The format of the Logs sent to the Logging server is specified in [POSH's GitHub](https://github.com/poshsecurity/Posh-SYSLOG).

## Code
The approach for the Logging through Python is done trhough the class ``CustomLogger``. This Class will take the global constants ``LOGGING_SERVER = "192.168.180.158", APPLICATION_NAME = "lab3-tcp-server-team5", UDP_PORT = "514", HOSTNAME = socket.gethostname() and P_ID = os.getpid()``.

The class gets instantiated (in this case, through the variable ``log``). Then you may use its methods to send relevant logs to the logging server. The methods available are ``send info | warning log(message)``. The argument ``message`` is a string that contains the message related to the log. Each of these methods then summons the parent method ``send_log(...)`` which will in parse all the data to a valid ``Send-SyslogMessage`` command.