# Lab Management script for ICS0020
# v1 / 2019.09.11
#       main functionality implemented, script used to provision teacher and students labs
#       check MAIN BODY for the logic        
# documentation https://code.vmware.com/web/tool/11.3.0/vmware-powercli
# Preparation
# NB! Run this code only once
# Comment out below after first run
# --start
# set environment variables
$user = "UNI_ID@ttu.ee"
$password = Read-Host -AsSecureString
# install all dependencies
InitialInstallation
# --end
# Virtual Center connection parameters
$vc = @{ `
    DNS                 = 'vcsa.itcollege.ee'; `
    Usr                 = $user; `
    Pwd                 = [Net.NetworkCredential]::new('', $password).Password `
    }
# Resource pool definition
$pool = @{ `
    Compute             = 'HPE BladeSystem Gen8 - Rack 3'; `
    Storage             = 'Hitachi_LUN1'; `
    Network             = 'VM Network'; `
    BaseLabLocation     = 'Labs.Students'; `
    BaseLabLocation2    = 'Labs.Course Development' `
    }
# List of templates
$tmpl = @{ `
    Lnx                 = 'Ubuntu 18.04 basic template v1.1'; `
    Win                 = 'Windows Server 2019 basic template v1.1'; `
}
# VM attributes
$vm = @{ `
    prfx                = 'ics0020-lab'; `
    WinTarget01         = 'managed-Node-Win-01'; `
    LnxTarget01         = 'managed-Node-Lnx-01'; `
    MgmtSrv01           = 'management-Server-01' `
}
Function InitialInstallation {
  # Installation
  Install-Module -Name Posh-SYSLOG
  # Update
  Update-Module -Name Posh-SYSLOG
  # Run to configure the environment
  # Needs to run just once
  Install-Module -Name VMware.PowerCLI
  Set-PowerCLIConfiguration -Scope User -ParticipateInCEIP $false
  Set-PowerCLIConfiguration -InvalidCertificateAction Ignore
}
Connect-VIServer $vc.DNS -user $vc.Usr -pass $vc.Pwd
$objtmpl = @{ `
    Lnx = Get-Template -Name $tmpl.Lnx; `
    Win = Get-Template -Name $tmpl.Win `
    }
$objclstDefault = Get-Cluster -Name $pool.Compute
$objstrgDefault = Get-Datastore -Name $pool.Storage
Function GetLabVMsIPs {
    For ($i=$a; $i -le $z; $i++) {
        $lab = $vm.prfx + "{0:00}" -f $i
        Get-VM ($lab + "*") | Select-Object Name, @{N="IP Address";E={@($_.guest.IPAddress[0])}} | Sort-Object Name | ConvertTo-Csv
    }    
}
Function RemoveLabVMs {
    For ($i=$a; $i -le $z; $i++) {
        $lab = $vm.prfx + "{0:00}" -f $i
        Remove-Folder -Folder $lab -DeletePermanently -Confirm:$false
    }    
}
Function StartLabVMs {
    For ($i=$a; $i -le $z; $i++) {
        $lab = $vm.prfx + "{0:00}" -f $i
        $labMgmtSrv01   = $lab + '.' + $vm.MgmtSrv01
        $labLnxTarget01 = $lab + '.' + $vm.LnxTarget01 
        $labWinTarget01 = $lab + '.' + $vm.WinTarget01 
        Start-VM -VM $labMgmtSrv01 -RunAsync
        Start-VM -VM $labLnxTarget01 -RunAsync
        Start-VM -VM $labWinTarget01 -RunAsync
    }    
}
Function StopLabVMs {
    For ($i=$a; $i -le $z; $i++) {
        $lab = $vm.prfx + "{0:00}" -f $i
        $labMgmtSrv01   = $lab + '.' + $vm.MgmtSrv01
        $labLnxTarget01 = $lab + '.' + $vm.LnxTarget01 
        $labWinTarget01 = $lab + '.' + $vm.WinTarget01 
        Stop-VM -VM $labMgmtSrv01 -RunAsync -Confirm:$false
        Stop-VM -VM $labLnxTarget01 -RunAsync -Confirm:$false
        Stop-VM -VM $labWinTarget01 -RunAsync -Confirm:$false
    }    
}     
Function ProvisionLabVMs {
    For ($i=$a; $i -le $z; $i++) {
        $lab = $vm.prfx + "{0:00}" -f $i
        New-Folder -Name $lab -Location $pool.BaseLabLocation
        $labMgmtSrv01   = $lab + '.' + $vm.MgmtSrv01
        $labLnxTarget01 = $lab + '.' + $vm.LnxTarget01 
        $labWinTarget01 = $lab + '.' + $vm.WinTarget01 
        New-VM -Name $labMgmtSrv01 `
            -Template $objtmpl.Lnx `
            -ResourcePool $objclstDefault `
            -Datastore $objstrgDefault `
            -Location $lab
        New-VM -Name $labLnxTarget01 `
            -Template $objtmpl.Lnx `
            -ResourcePool $objclstDefault `
            -Datastore $objstrgDefault `
            -Location $lab
            New-VM -Name $labWinTarget01 `
            -Template $objtmpl.Win `
            -ResourcePool $objclstDefault `
            -Datastore $objstrgDefault `
            -Location $lab
    }
}
