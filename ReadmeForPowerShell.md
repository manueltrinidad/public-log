First of all in order to send log message to the target server, Posh-SYSLOG must be installed onto the host machine. Since this is the module for sending syslog messages from PowerShell.

```
$user = "UNI_ID@ttu.ee"
$password = Read-Host -AsSecureString

# install all dependencies
InstallDepend

$vc = @{ `
    DNS                 = 'vcsa.itcollege.ee'; `
    Usr                 = $user; `
    Pwd                 = [Net.NetworkCredential]::new('', $password).Password `
    }
    
# Resource pool dictionary
$pool = @{ `
    Compute             = 'HPE BladeSystem Gen8 - Rack 3'; `
    Storage             = 'Hitachi_LUN1'; `
    Network             = 'VM Network'; `
    BaseLabLocation     = 'Labs.Students'; `
    BaseLabLocation2    = 'Labs.Course Development' `
    }
    
# templates
$tmpl = @{ `
    Lnx                 = 'Ubuntu 18.04 basic template v1.1'; `
    Win                 = 'Windows Server 2019 basic template v1.1'; `
}

# Host variables
$vm = @{ `
    prfx                = 'ics0020-lab'; `
    WinTarget01         = 'managed-Node-Win-01'; `
    LnxTarget01         = 'managed-Node-Lnx-01'; `
    MgmtSrv01           = 'management-Server-01' `
}
```

Here we are setting variables, user which is going to be hostname variable must be provided for log message, since it is the requirement for RFC5424. `InstallDepend` is a function which will be explained later in the README.

<br>

```
}
Function InstallDepend {
  # Install
  Install-Module -Name Posh-SYSLOG
  # Update
  Update-Module -Name Posh-SYSLOG
  # Configuration
  # Need only once
  Install-Module -Name VMware.PowerCLI
  Set-PowerCLIConfiguration -Scope User -ParticipateInCEIP $false
  Set-PowerCLIConfiguration -InvalidCertificateAction Ignore
}
```

Here we have a function for `InstallDepend`. This function make sure to install Psh-SYSLOG, update and configure the  module. Once it’s ran, no need to be executed in future ran. Since once it is ran, there will be no need for another installation and configuration for Posh-SYSLOG, unless it is deleted or the configuration file has been modified.

<br>

```
Function GetLabVMsIPs {
    For ($i=$a; $i -le $z; $i++) {
        $lab = $vm.prfx + "{0:00}" -f $i
        Get-VM ($lab + "*") | Select-Object Name, @{N="IP Address";E={@($_.guest.IPAddress[0])}} | Sort-Object Name | ConvertTo-Csv
    }    
}
```

Here is the function for retrieving the IP address of the host where this script is  on.

<br>

```
Function StartLabVMs {
    For ($i=$a; $i -le $z; $i++) {
        $lab = $vm.prfx + "{0:00}" -f $i
        $labMgmtSrv01   = $lab + '.' + $vm.MgmtSrv01
        $labLnxTarget01 = $lab + '.' + $vm.LnxTarget01 
        $labWinTarget01 = $lab + '.' + $vm.WinTarget01 
        Start-VM -VM $labMgmtSrv01 -RunAsync
        Start-VM -VM $labLnxTarget01 -RunAsync
        Start-VM -VM $labWinTarget01 -RunAsync
    }    
}
```

Here is the function for starting the host machine. It is starting all management Linux server, managed Linux and managed Windows machines.

<br>

```
Function StopLabVMs {
    For ($i=$a; $i -le $z; $i++) {
        $lab = $vm.prfx + "{0:00}" -f $i
        $labMgmtSrv01   = $lab + '.' + $vm.MgmtSrv01
        $labLnxTarget01 = $lab + '.' + $vm.LnxTarget01 
        $labWinTarget01 = $lab + '.' + $vm.WinTarget01 
        Stop-VM -VM $labMgmtSrv01 -RunAsync -Confirm:$false
        Stop-VM -VM $labLnxTarget01 -RunAsync -Confirm:$false
        Stop-VM -VM $labWinTarget01 -RunAsync -Confirm:$false
    }    
}
```

Here is the function for stopping the host machine. It is starting all management Linux server, managed Linux and managed Windows machines.
