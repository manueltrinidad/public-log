import os
import socket
import subprocess as sp
import datetime
from signal import signal, SIGINT

LOGGING_SERVER = "192.168.180.158"
APPLICATION_NAME = "lab3-tcp-server-team5"
UDP_PORT = "514"
HOSTNAME = socket.gethostname()
P_ID = os.getpid()

TCP_IP = "0.0.0.0"
TCP_PORT = 12345


class CustomLogger:
    @staticmethod
    def send_log(message, severity, facility, timestamp=datetime.datetime.now(), p_id=P_ID, message_id="-",
                 server=LOGGING_SERVER, hostname=HOSTNAME, application_name=APPLICATION_NAME, udp_port=UDP_PORT):
        ps_command = 'Send-SyslogMessage -Server "{0}" -Message "{1}" -Severity "{2}" -Facility "{3}" -UDPPort "{4}" ' \
                     '-ApplicationName "{5}" -Hostname "{6}" -ProcessID "{7}"'.format(server, message, severity,
                                                                                      facility, udp_port,
                                                                                      application_name, hostname, p_id)
        print(ps_command)
        sp.call(["C:\\WINDOWS\\system32\\WindowsPowerShell\\v1.0\\powershell.exe", ps_command])

    def send_info_log(self, message):
        severity = "Informational"
        facility = 'syslog'
        self.send_log(message, severity, facility)

    def send_warning_log(self, message):
        severity = "Warning"
        facility = "syslog"
        self.send_log(message, severity, facility)


# A function that handles interrupt signal by sending out a log
def sigint_handler():
    log.send_warning_log("Program execution ended.")
    print("Exiting TCP Server.")
    raise SystemExit(0)


if __name__ == '__main__':
    # Setup Logging
    log = CustomLogger()

    # Setup TCP Server
    tcpSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcpSocket.bind((TCP_IP, TCP_PORT))
    tcpSocket.listen(5)

    serverStartedMsg = "Started TCP Server on {} port {}".format(TCP_IP, TCP_PORT)
    log.send_info_log(serverStartedMsg)
    print(serverStartedMsg)

    # bind handler to SIGINT signal
    signal(SIGINT, sigint_handler)
    while True:
        # Accept connection, returns new client socket object and client/remote address
        clientConnection, clientAddress = tcpSocket.accept()

        print(f"Accepted connection from {clientAddress}")
        # Receive data from the socket, parameter specifies the buffer size, return value is byte object
        buffer = clientConnection.recv(1024)
        # Sends the byte object back to client
        clientConnection.send(buffer)
        log.send_info_log("Get Request from {}".format(clientAddress))
        clientConnection.close()
