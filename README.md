# group5

### Logging specifications for lab3-itcollege-vm-mgmt.ps and lab3-tcp-server.py

* format:
  * syslog RFC5424
* FACILITY: 1 (user-level messages)
* SEVERITY: 3 to 7
  * 3 - Error: Error conditions
  * 4 - Warning: Warning conditions
  * 5 - Notice: Normal but significant condition
  * 6 - Informational: Informational messages
  * 7 - Debug: Debug-level messages
* TIMESTAMP: yes
* HOSTNAME: yes, labXXlnx
* APPLICATION NAME: yes, script or application name
* PROCESS ID: optional
* MESSAGE ID: optional
* MESSAGE: actual event message

### Log message format

<F*S>1 HOSTNAME APPLICATION-NAME PROCESS-ID MESSAGE-ID TIMESTAMP MESSAGE